{
  description = "kind-generics flake";

  inputs = {
    nixpkgs.url = "flake:nixpkgs/nixos-23.05";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        # The argument with-hls decides whether the haskell-language-server is included. This is
        # disabled by default, because it is not always on cachix and can take quite a while to build.
        makeDevShell = {with-hls ? false, ghcVersion}: pkgs.mkShell {
          packages = with pkgs; [
            haskell.compiler."ghc${ghcVersion}"
            (if with-hls
             then haskell-language-server.override {supportedGhcVersions = [ ghcVersion ];}
             else null)
            cabal-install
          ];
        };
    in {
        devShell927 = makeDevShell {with-hls = true; ghcVersion = "927"; };
        devShell944 = makeDevShell {with-hls = false; ghcVersion = "944"; };
        devShell = self.outputs.devShell927.${system};
      }
    );
}
